# Changelog

## v0.0.1 -> v0.0.2

* Added pages to add schedule
* can add multiple medicines to schedule

## v0.0.2 -> v0.0.3

* Added : remove field option while adding schedule
* Added : Option to navigate schedule by date
* Added : Ability to retrieve records only relavent to the date selected
* Added : Ability to delete from schedule
* Added : Swipe to delete
* Removed : JavaScript date function, replaced with MomentJS date function
* Minor cosmetic changes

## v0.0.3 -> v0.0.4

* Added : Search Functionality
* Added : Random Sample of 1000/84129 medicines to Mongo Cloud
* Created : Search Index for medicine sample
* Added : Working Search Results page with Add To Cart button
* Added : Cart Provider to maintain cart
* Added : Cart functionalities to add medicine. If medicine is added twice, quantity is updated
* Added : Cart functionality to remove medicines (Must be handled in front end)
* Added : Cart functionality to increase / decrease medicine quantity. If quantity is decreased to 0, medicine vanishes from cart (Add warning)
* Added : Dynamic total computation in cart


## v0.0.4 -> v0.0.5

* Added : Cart Accesible across headers
* Added : Checkout Page from Cart
* Added : Saving a prescription from Camera and Gallery works, creates folder in server
* Added : Native File Transfer module support to navigate, display and upload files from File System (Android/iOs)
* Added : Native Camera Module support to click pictures using the camera of the phone
* Added : View Prescription Page
* Added : Image Load support from File System
* Added : Feature to name a prescription while uploading

## v0.0.5 -> v0.0.6

* Fixed : Cordova Dependency issues / Gradle build issues for Android Platform

## v0.0.6 -> v0.0.7

* Added : Patient provider to interact with API to get/add address in a particular format
* Added : Choose Address Page after finalizing medicines. This page gets addresses of a patient and lets the patient choose a address
* Added : Add Address Page with Geocoder support for automatic address filling and GPS support for location identification on Google Maps
* Modified : Post checkout, instead of broadcasting, the patient can now choose an address where they want the medicine delivered so that its easier to connect to partners near to the delivery address
* Fixed : Module dependency issues, removed *@5.x.x modules and replaced it with *@4.x.x modules for better support. Fixed status bar hiding issue

## v0.0.7 -> v0.0.8

* Completed Broadcast route
* Added  Option to name a prescription on checkout
* Added : View Bids page and button on homepage to view active / unseen bids only
* Added : Provider Support (Request) for Checkout Function that broadcasts prescription to pharmarcists based on the city of the address selected
* Added : Provider Support (Bidding) to get active bids (Requests that have been broadcasted and are awaiting bids / have some bids)
* Added : Provider Support (Bidding) to accept a bid for a particular request
* Added : New Page to view requests (Requests are created when meds are added to cart and delivery address is chosen, bids are recieved for a request), fully functional with fetching data to server
* Added : New Page to view proforma invoice of a particular request after broadcast; fully functional with capability to fetch data from server
* Added : Page to view bids for a particular Request; one of the many bids can be accepted from here based on price; fully functional with server-side integration
* Modified : Removed unnecessary import statements
* Modified : Login functionality to make use of Javascript WebToken to skip login if a JWT is active.
* Modified : Home page to add button to view active requests
* Removed : Unnecessary console logging added for debugging.

## v0.0.8 -> v0.0.9

* Added : Order Provider to get Active Orders of patient
* Added : Ability to view order summary from orders page
* Added : Track Page to track order status
* Implemented variable changes made in API for correctness

## v0.0.9 -> v0.0.10

* Added : Request by Image provider so patient can broadcast a prescription image
* Added : Choose address / broadcast option in View Prescription page to allow broadcasting a saved prescription
* Added : Choose address page when Image-based prescription is chosen
* Modified : View Proforma Invoice moved from View Requests Page to View Bids Page, so every bid will have its designated proforma invoice.
* Modified : Nomenclature in view-proforma-invoice page for correctness

## v0.0.10 -> v0.0.11

* Modified : UI color scheme for all pages.
* Modified : Alignment of design components.

## v0.0.11 -> v1.0.0

* Added : Provider to get all patient orders instead of only active orders
* Added : Conditional viewing of proforma invoice, only fields that have info are shown
* Added : Viewing Dispatch Time/Date and Delivery Options in a bid
* Added : Confirm alert added before placing a bid
* Added : Toast to inform successfull addition of medicine to cart from a search page
* Added : Validation in the schedule-add page, instructions must be numberic and length is restricted to 3.
* Added : Conditional display of fields in Schedule add page (Fields are displayed based on value in the instructions field)
* Added : Orders can now be filtered between Active and Past
* Added : Option to mark order as urgent and signify expected delivery
* Added : Confirm alert & Form Validation on Image Broadcast page, Broadcast page and Choose-address page
* Fixed : Cart shows "No Items in Cart" message instead of "Total : 0" when no items are in cart
* Fixed : Formatting issue in schedule page
* Fixed : After placing a bid, navigate to homepage instead of stay on the same page.
* Fixed : Bug where cart items would still be present even after placing an order
* Removed : Unnecessary details from View Requests Page

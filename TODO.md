* Toast All Errors
* Validation on Schedule 2 add page
* Make Instructions in Schedule 1 page to 3 input fields for 111 or 101 like instructions
* Test Schedule Addition - DONE
* Add validation for either morning, afternoon or night time. Ideally, spawn input only when needed.
* Add loaders everywhere

## Cart

* Add a warning when quantity is becoming 0, before removing from cart
* Add an option to view the cart from header of every page - DONE
* Add a indication of number of content in the cart, beside the cart icon
* Checkout options from cart - DONE
* Remove console.log everywhere 
* Loaders

## Image

* Write GET routes - DONE

## Orders

* Write API routes for Orders - DONE

## View Prescription

* Can't view image. There is an error which might be because of port forwarding. - FIXED

## Broadcast Choose Address

* Remove null values from Database and check if the get route works
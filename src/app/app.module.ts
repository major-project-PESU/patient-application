import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 

import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/';

import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { SchedulePage } from '../pages/schedule/schedule';
import { OrdersPage } from '../pages/orders/orders';
import { ProfilePage } from '../pages/profile/profile';
import { LandingPage } from '../pages/landing/landing';
import { ScheduleAddPage } from '../pages/schedule-add/schedule-add';
import { AuthProvider } from '../providers/auth/auth';
import { ScheduleProvider } from '../providers/schedule/schedule';
import { Schedule_2AddPage } from '../pages/schedule-2-add/schedule-2-add';
import { SearchProvider } from '../providers/search/search';
import { SearchResultsPage } from '../pages/search-results/search-results';
import { CartPage } from '../pages/cart/cart';
import { CartProvider } from '../providers/cart/cart';

import { Camera } from '@ionic-native/camera/';
import { FileTransfer } from '@ionic-native/file-transfer';
import { CheckoutPage } from '../pages/checkout/checkout';
import { OrderProvider } from '../providers/order/order';
import { RequestProvider } from '../providers/request/request';
import { ViewPrescriptionPage } from '../pages/view-prescription/view-prescription';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { BroadcastChooseAddressPage } from '../pages/broadcast-choose-address/broadcast-choose-address';
import { BroadcastAddAddressPage } from '../pages/broadcast-add-address/broadcast-add-address';
import { PatientProvider } from '../providers/patient/patient';
import { ViewBidsPage } from '../pages/view-bids/view-bids';
import { BiddingProvider } from '../providers/bidding/bidding';
import { ViewRequestsPage } from '../pages/view-requests/view-requests';
import { ViewProformaCheckoutPage } from '../pages/view-proforma-checkout/view-proforma-checkout';
import { TrackPage } from '../pages/track/track';
import { ImageChooseAddressPage } from '../pages/image-choose-address/image-choose-address';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    SchedulePage,
    OrdersPage,
    ProfilePage,
    LandingPage,
    ScheduleAddPage,
    RegisterPage,
    Schedule_2AddPage,
    SearchResultsPage,
    CartPage,
    CheckoutPage,
    ViewPrescriptionPage,
    BroadcastChooseAddressPage,
    BroadcastAddAddressPage,
    ViewBidsPage,
    ViewRequestsPage,
    ViewProformaCheckoutPage,
    TrackPage,
    ImageChooseAddressPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    SchedulePage,
    OrdersPage,
    ProfilePage,
    LandingPage,
    ScheduleAddPage,
    RegisterPage,
    Schedule_2AddPage,
    SearchResultsPage,
    CartPage,
    CheckoutPage,
    ViewPrescriptionPage,
    BroadcastChooseAddressPage,
    BroadcastAddAddressPage,
    ViewBidsPage,
    ViewRequestsPage,
    ViewProformaCheckoutPage,
    TrackPage,
    ImageChooseAddressPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthProvider,
    Geolocation,
    ScheduleProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SearchProvider,
    CartProvider,
    Camera,
    FileTransfer,
    OrderProvider,
    NativeGeocoder,
    RequestProvider,
    PatientProvider,
    BiddingProvider
  ]
})
export class AppModule {}

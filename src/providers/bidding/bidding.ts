import {
  Http,
  Headers
} from '@angular/http';
import 'rxjs/add/operator/map';
import {
  CONFIG
} from '../../../config/config';

import {
  AuthProvider
} from '../auth/auth';
import { Injectable } from '@angular/core';

@Injectable()
export class BiddingProvider {

  public API_URL:any;
  public token:any;
  constructor(public http: Http, public authServ:AuthProvider) {
    this.API_URL = CONFIG['API_URL'];
    this.token = this.authServ.token;
  }

  patientGetPresentBids(){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append("Content-type",'application/json');

      this.http.post(this.API_URL + "/patient/bids/present", JSON.stringify({}), {headers:headers})
      .subscribe((res) => {
        resolve(res.json());
      }, (err) => {
        reject(err)
      })
    })
  }

  patientAccept(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append("Content-type",'application/json');

      this.http.post(this.API_URL + "/patient/bid/accept", JSON.stringify(details), {headers:headers})
      .subscribe((res) => {
        resolve(res.json());
      }, (err) => {
        reject(err)
      })
    })
  }

}

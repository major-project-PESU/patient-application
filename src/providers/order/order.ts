import {
  Http,
  Headers
} from '@angular/http';
import 'rxjs/add/operator/map';
import {
  CONFIG
} from '../../../config/config';

import {
  AuthProvider
} from '../auth/auth';
import { Injectable } from '@angular/core';

/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderProvider {

  public orders = [
    {
      _id:"idForOrder1",
      orderName:"Broadcasted",
      time:"09:00",
      date:"28/02/2019",
      requestId:"reqId",
      status:"Bid",
      latestUpdate:"5 Bids"
    },
    {
      _id:"idForOrder1",
      orderName:"Active",
      time:"09:00",
      date:"28/02/2019",
      requestId:"reqId",
      status:"Active",
      latestUpdate:"Ready for Pickup"
    },
    {
      _id:"idForOrder1",
      orderName:"ABC",
      time:"09:00",
      date:"28/02/2019",
      requestId:"reqId",
      status:"Past",
      latestUpdate:"Fulfilled on 03/03/2019"
    },
  ]

  public token:any;
  public API_URL:any;
  constructor(public http: Http, public authServ:AuthProvider) {
    this.token = this.authServ.token;
    this.API_URL = CONFIG['API_URL'];
    console.log('Hello OrderProvider Provider');
  }

  patientGetAllOrders(){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append('Content-type','application/json');
      this.http.post(this.API_URL + "/patient/orders/all",JSON.stringify({}),{headers:headers})
      .subscribe((res) => {
        resolve(res.json())
      }, (err) => {
        reject(err)
      })
    })
  }
  patientGetActiveOrders(){
    // return this.orders;
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append("Authorization",this.token);
      headers.append('Content-type','application/json');
      this.http.post(this.API_URL + '/patient/orders/active',JSON.stringify({}),{headers:headers})
      .subscribe((res) => {
        resolve(res.json())
      }, (err) => {
        reject(err)
      })
    })
  }

}

import {
  Injectable
} from '@angular/core';
import {
  Http,
  Headers
} from '@angular/http';
import {
  Storage
} from '@ionic/storage';
import 'rxjs/add/operator/map';

import {
  CONFIG
} from '../../../config/config';

import {
  AuthProvider
} from '../auth/auth';

@Injectable()
export class ScheduleProvider {
  public token:any;
  public API_URL : any;
  constructor(
    public http: Http, 
    public storage: Storage, 
    public authService: AuthProvider ) 
  {

    this.API_URL = CONFIG['API_URL'];
    this.token = authService.token;
  }

  getSchedule(){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);

      this.http.get(this.API_URL+'/schedule',{headers:headers})
      .subscribe(res => {
        let data = res.json();
        resolve(data);
      }, err => {
        reject(err)
      })
    })
  }

  getScheduleByDate(date){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);

      this.http.get(this.API_URL+'/schedule/'+date,{headers:headers})
      .subscribe(res => {
        let data = res.json();
        resolve(data);
      }, err => {
        reject(err)
      })
    })
  }

  addMedicine(medicines){

    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Content-type','application/json');
      headers.append('Authorization',this.token);
  
      this.http.post(this.API_URL+'/schedule/add',JSON.stringify(medicines),{headers:headers})
      .subscribe((res) => {
        let data = res.json();
        resolve(data);
      }, (err) => {
        reject(err)
      })
    })

  }

  deleteMedicine(medicine){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Content-type','application/json');
      headers.append('Authorization',this.token);
  
      this.http.post(this.API_URL+'/schedule/delete',JSON.stringify(medicine),{headers:headers})
      .subscribe((res) => {
        let data = res.json();
        resolve(data);
      }, (err) => {
        reject(err)
      })
    })
  }


}

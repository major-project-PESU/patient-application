import {
  HttpClient
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';


@Injectable()
export class CartProvider {

  public medicines: any = [

    // {
    //   _id:"5c8a001f236c914292b9f7f5",
    //   Name:"Numlo-AT 25 (5mg/25mg)",
    //   Company:"Emcure Pharmaceuticals Ltd.",
    //   Type:"Tablet",
    //   "Generic Name":"Amlodipine",
    //   Dosage:"5mg/25mg",
    //   Quantity:10,
    //   Price:15,
    //   Units:0,
    //   Total:0
    // },
    // {
    //   _id:"5c8a001f236c914292b9f7f5",
    //   Name:"Med 2",
    //   Company:"Emcure Pharmaceuticals Ltd.",
    //   Type:"Tablet",
    //   "Generic Name":"Amlodipine",
    //   Dosage:"5mg/25mg",
    //   Quantity:10,
    //   Price:15,
    //   Units:0,
    //   Total:0
    // }
  ];
  public name:any;
  public total: any = 0;
  constructor(public http: HttpClient) {
    console.log('Hello CartProvider Provider');
  }

  reset(){
    this.name = "";
    this.total = 0;
    this.medicines = []
  }
  addMedicine(medicine) {
    for(var i in this.medicines){
      if(this.medicines[i].Name === medicine.Name){
        this.increaseQty(i);
        return 1;
      }
    }
    medicine['Units'] = 1;
    medicine['Total'] = medicine['Price'] * medicine['Units']
    this.total += medicine['Total']
    this.medicines.push(medicine);
    console.log(this.medicines)
  }

  removeMedicine(index) {
    this.medicines.splice(index, 1);
  }

  decreaseQty(index) {
    if (this.medicines[index]['Units'] - 1 >= 0) {
      this.medicines[index]['Units']--;
      this.total -= this.medicines[index]['Total']
      this.medicines[index]['Total'] = this.medicines[index]['Price'] * this.medicines[index]['Units'];
      this.total += this.medicines[index]['Total']
      if(this.total < 0.5){
        this.total=0;
      }
    }
    if(this.medicines[index]['Units'] == 0){
      this.medicines.splice(index,1);
    }
    
  }

  increaseQty(index){
    this.medicines[index]['Units']++;
    this.total -= this.medicines[index]['Total'];
    this.medicines[index]['Total'] = this.medicines[index]['Price'] * this.medicines[index]['Units'];
    this.total += this.medicines[index]['Total']


  }
}

import {
  Http,
  Headers
} from '@angular/http';
import {
  Storage
} from '@ionic/storage';
import 'rxjs/add/operator/map';

import {
  CONFIG
} from '../../../config/config';

import {
  AuthProvider
} from '../auth/auth';
import { Injectable } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@Injectable()
export class RequestProvider {

  public API_URL:any;
  public token:any;
  constructor(    
    public http: Http, 
    public storage: Storage, 
    public authService: AuthProvider ,
    private transfer: FileTransfer) {
    // console.log('Hello RequestProvider Provider');


    this.API_URL = CONFIG['API_URL'];
    this.token = authService.token;
  }

  getRecords(){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);

      this.http.get(this.API_URL+"/prescription/get", {headers:headers})
      .subscribe(res => {
        let data = res.json();
        resolve(data)
      }, err => {
        reject(err)
      })
    })
  }

  save(img,name){
    var targetPath = img;
    var params = new Object();
    params['headers'] = {"Authorization":this.token};
    params['name'] = name;
    var url = this.API_URL + "/prescription/save"
    var options: FileUploadOptions = {
      fileKey: 'image',
      chunkedMode: false,
      mimeType: 'multipart/form-data',
      params:params,
      
    };
    // console.log("Saving now");
 
    const fileTransfer: FileTransferObject = this.transfer.create();
 
    // Use the FileTransfer to upload the image
    return fileTransfer.upload(targetPath, url, options);
  }

  checkout(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append('Content-type','application/json');
      this.http.post(this.API_URL+"/request/checkout",JSON.stringify(details),{headers:headers})
      .subscribe((res) => {
        resolve(res.json())
      }, (err) => {
        reject(err)
      })
    })
  }

  checkoutByImage(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization', this.token);
      headers.append('Content-type','application/json');
      this.http.post(this.API_URL+"/request/checkout/image",JSON.stringify(details),{headers:headers})
      .subscribe((res) => {
        resolve(res.json())
      }, err => {
        reject(err)
      })
    })
  }
}

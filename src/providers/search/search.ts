import {
  Injectable
} from '@angular/core';
import {
  Http,
  Headers
} from '@angular/http';
import {
  Storage
} from '@ionic/storage';
import 'rxjs/add/operator/map';

import {
  CONFIG
} from '../../../config/config';

import {
  AuthProvider
} from '../auth/auth';
/*
  Generated class for the SearchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SearchProvider {

  public API_URL:any;
  public token:any;
  constructor( public http: Http, 
    public storage: Storage, 
    public authService: AuthProvider) {

    this.API_URL = CONFIG['API_URL'];
    this.token = authService.token;
  }

  search(med){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append('Content-type','application/json');
      let json = {
        "name":med
      }
      this.http.post(this.API_URL+"/medicine/search",JSON.stringify(json),{headers:headers})
      .subscribe((res) => {
        let data = res.json()
        resolve(data)
      }, (err) => {
        reject(err)
      })
    })
  }

}

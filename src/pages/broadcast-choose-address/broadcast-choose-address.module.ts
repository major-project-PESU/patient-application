import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BroadcastChooseAddressPage } from './broadcast-choose-address';

@NgModule({
  declarations: [
    BroadcastChooseAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(BroadcastChooseAddressPage),
  ],
})
export class BroadcastChooseAddressPageModule {}

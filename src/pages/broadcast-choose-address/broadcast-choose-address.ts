import {
  Component,
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';

import { BroadcastAddAddressPage } from '../broadcast-add-address/broadcast-add-address';
import { PatientProvider } from '../../providers/patient/patient';
import { RequestProvider } from '../../providers/request/request';
import { CartProvider } from '../../providers/cart/cart';
import { LandingPage } from '../landing/landing';
/**
 * Generated class for the BroadcastChooseAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-broadcast-choose-address',
  templateUrl: 'broadcast-choose-address.html',
})
export class BroadcastChooseAddressPage {

  public addresses = [];
  public urgent: any;
  public by:any;
  public address:any;
  public valid = false;
  constructor(public navCtrl:NavController, public alertCtrl:AlertController, public navParams:NavParams, public patientServ:PatientProvider, public requestServ:RequestProvider,
    public cartServ:CartProvider){

  }

  getAddress(){
    this.patientServ.getAddress().then((res) => {
      this.addresses = [];
      for(var i in res){
        this.addresses.push(res[i])
      }
    }, (err) => {
      console.log(err)
    })
  }
  

  confirmAlert(){
    let alert = this.alertCtrl.create({
      title:"Confirm Order",
      message:"Do you wish to confirm the order details and place an order? Note : This can't be undone",
      buttons : [
        {
          text: "No",
          role:'cancel',

        },
        {
          text: "Yes",
          handler: () => {
            this.broadcast();
          }
        }
      ]
    });
    alert.present();
  }
  
  isValid(){
    // console.log("VALIDATING");
    this.valid = false;
    if(!this.urgent){
      if(this.address){
        this.valid = true;
      }
    }else{
      if(this.address && this.by){
        this.valid = true
      }
    }
  }

  broadcast(){
    let details = {
      name:this.cartServ.name,
      medicines:this.cartServ.medicines,
      address : this.address,
      total:this.cartServ.total,
      urgent:this.urgent,
      by:this.by
    }
    this.requestServ.checkout(details).then((res) => {
      this.cartServ.reset();
      this.navCtrl.setRoot(LandingPage)
    },(err) => {
      console.log(err)
    })
  }
  goToAddAddressPage(){
    this.navCtrl.push(BroadcastAddAddressPage);
  }


  ionViewDidEnter(){
    this.getAddress();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad BroadcastChooseAddressPage');
  }

}

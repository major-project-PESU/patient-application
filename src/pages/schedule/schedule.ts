import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ScheduleAddPage } from '../schedule-add/schedule-add';
import { ScheduleProvider } from '../../providers/schedule/schedule';
import * as moment from 'moment';
import { CartPage } from '../cart/cart';
/**
 * Generated class for the SchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {
  public medicines = [];
  public date: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public scheduleServ:ScheduleProvider) {
    this.date = moment().format("DD MMM YYYY");
  }

  nextDate(){
    this.date = (moment(this.date).add(1,'day')).format("DD MMM YYYY");
    this.getSchedule();
  }

  previousDate(){
    this.date = (moment(this.date).subtract(1,'day')).format("DD MMM YYYY")
    this.getSchedule();
  }

  delete(med){
    // console.log(med)
    let json = {
      medicineID:med._id
    }
    this.scheduleServ.deleteMedicine(json).then((res) => {
      this.getSchedule();
    }, (err) => {
      console.log(err)
    })
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad SchedulePage');
    this.getSchedule();
  }

  getSchedule(){
    var dateFormat = (moment(this.date).format('YYYY/MM/DD'));

    this.medicines = [];
    this.scheduleServ.getScheduleByDate(dateFormat).then((res) => {
      for(var i in res){
        this.medicines.push(res[i])
      }
      console.log(this.medicines)
    }, (err) => {
      console.log(err)
    })
  }
  goToAddMedPage(){
    this.navCtrl.push(ScheduleAddPage);
  }

  goToCart(){
    this.navCtrl.push(CartPage);
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { RequestProvider } from '../../providers/request/request';
import { ViewPrescriptionPage } from '../view-prescription/view-prescription';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public records : any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public requestServ:RequestProvider) {
  }

  getRecords(){
    this.requestServ.getRecords().then((res) => {
      this.records = res;
    }, (err) => {
      console.log(err);
    })
  }

  goToViewPrescription(image){
    // console.log(imageUrl)
    this.navCtrl.push(ViewPrescriptionPage, {image:image})
  }
  goToCart(){
    this.navCtrl.push(CartPage);
  }
  ionViewDidEnter() {
    console.log('ionViewDidLoad ProfilePage');
    this.getRecords()
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BiddingProvider } from '../../providers/bidding/bidding';
import { LandingPage } from '../landing/landing';
import { ViewProformaCheckoutPage } from '../view-proforma-checkout/view-proforma-checkout';
import * as moment from 'moment';
/**
 * Generated class for the ViewBidsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-bids',
  templateUrl: 'view-bids.html',
})
export class ViewBidsPage {

  public request :any = []
  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public navParams: NavParams, public biddingServ:BiddingProvider) {
    this.request = this.navParams.get('request')
    for(var i in this.request.bids){
      this.request.bids[i].dispatchDate = moment(this.request.bids[i].dispatchDate).format("DD MMM")
    }
    console.log(this.request)
  }
  goToViewProformaCheckout(bid){
    this.navCtrl.push(ViewProformaCheckoutPage,{bid:bid});
  }
  

  confirmAccept(bid){
    let alert = this.alertCtrl.create({
      title:"Confirm Order",
      message:"Accept bid from " +bid.pharmacyName+ ". Note : This can't be undone",
      buttons : [
        {
          text: "No",
          role:'cancel',

        },
        {
          text: "Yes",
          handler: () => {
            this.accept(bid);
          }
        }
      ]
    });
    alert.present();   
  }
  accept(bid){
    let json = {
      "bidID":bid._id
    };
    this.biddingServ.patientAccept(json).then((res) => {
      this.navCtrl.setRoot(LandingPage)
      console.log(res)
    }, (err) => {
      console.log(err);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewBidsPage');
  }

}

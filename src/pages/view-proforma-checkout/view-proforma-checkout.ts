import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ViewProformaCheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-proforma-checkout',
  templateUrl: 'view-proforma-checkout.html',
})
export class ViewProformaCheckoutPage {

  public bid :any = []
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.bid = this.navParams.get('bid');
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewProformaCheckoutPage');
  }

}

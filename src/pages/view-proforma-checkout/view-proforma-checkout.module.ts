import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewProformaCheckoutPage } from './view-proforma-checkout';

@NgModule({
  declarations: [
    ViewProformaCheckoutPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewProformaCheckoutPage),
  ],
})
export class ViewProformaCheckoutPageModule {}

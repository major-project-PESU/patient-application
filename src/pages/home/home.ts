import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SearchProvider } from '../../providers/search/search';
import { SearchResultsPage } from '../search-results/search-results';
import { CartPage } from '../cart/cart';
import { ViewRequestsPage } from '../view-requests/view-requests';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  public searchTerm:any;
  constructor(public navCtrl: NavController, public searchServ : SearchProvider) {
    
  }

  goToCart(){
    this.navCtrl.push(CartPage);
  }

  goToViewBids(){
    this.navCtrl.push(ViewRequestsPage)
  }

  search(){
    this.searchServ.search(this.searchTerm).then((res) => {
      this.navCtrl.push(SearchResultsPage,{data:res})
    }, (err) => {
      console.log(err)
    })
  }

  
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RequestProvider } from '../../providers/request/request';

/**
 * Generated class for the UploadModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upload-modal',
  templateUrl: 'upload-modal.html',
})
export class UploadModalPage {
  public imageData:any;
  public desc:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public requestServ:RequestProvider) {
    this.imageData = this.navParams.get('data');
  }

  save(){
    this.requestServ.save(this.imageData, this.desc);
    this.navCtrl.pop();
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadModalPage');
  }

}

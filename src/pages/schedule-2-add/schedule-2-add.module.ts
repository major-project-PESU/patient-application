import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Schedule_2AddPage } from './schedule-2-add';

@NgModule({
  declarations: [
    Schedule_2AddPage,
  ],
  imports: [
    IonicPageModule.forChild(Schedule_2AddPage),
  ],
})
export class Schedule_2AddPageModule {}

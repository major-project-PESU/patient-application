import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
} from 'ionic-angular';
import { ScheduleProvider } from '../../providers/schedule/schedule';

@IonicPage()
@Component({
  selector: 'page-schedule-2-add',
  templateUrl: 'schedule-2-add.html',
})
export class Schedule_2AddPage {
  public data: any;
  public medicines: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public scheduleServ:ScheduleProvider) {
    this.data = this.navParams.get('data');
    this.medicines = this.data['medicines'];
    for (var i in this.medicines) {
      this.medicines[i]['days'] = (this.createList());
      this.medicines[i]['selectedDays'] = [];
    }

  }

  createList() {
    return [{
        testID: 0,
        name: "Sun",
        checked: false
      },
      {
        testID: 1,
        name: "Mon",
        checked: false
      },
      {
        testID: 2,
        name: "Tue",
        checked: false
      },
      {
        testID: 3,
        name: "Wed",
        checked: false
      },
      {
        testID: 4,
        name: "Thur",
        checked: false
      },
      {
        testID: 5,
        name: "Fri",
        checked: false
      },
      {
        testID: 6,
        name: "Sat",
        checked: false
      }
    ]

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Schedule_2AddPage');
  }

  selectMember(data, selectedArray) {
    if (data.checked == true) {
      selectedArray.push(data.testID);
    } else {
      for (var i in selectedArray) {
        if (selectedArray[i] == data.testID) {
          selectedArray.splice(i, 1)
        }
      }
    }
  }

  submit(){
    let json = {
      "medicines":this.medicines
    }
    this.scheduleServ.addMedicine(json).then((res) => {
      this.navCtrl.pop();
      this.navCtrl.pop();
    }, (err) => {
      console.log(err)
    })
  }
}

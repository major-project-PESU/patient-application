import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { CartPage } from '../cart/cart';

/**
 * Generated class for the SearchResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-results',
  templateUrl: 'search-results.html',
})
export class SearchResultsPage {

  public results:any = []
  constructor(public navCtrl: NavController, public navParams: NavParams, public cartServ:CartProvider, public toastCtrl:ToastController) {
     this.results = this.navParams.get('data');
  }

  addedToCartToast(name) {
    let toast = this.toastCtrl.create({
      message: "Added to cart "+name,
      duration: 1000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SearchResultsPage');
  }

  goToCart(){
    this.navCtrl.push(CartPage);
  }

}

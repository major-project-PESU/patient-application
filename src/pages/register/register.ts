import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder} from '@angular/forms';
// import { FormArray, FormGroup, Validators } from '@angular/forms'
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage {

  public form:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _FB          : FormBuilder,
    public authServ : AuthProvider
    
    ) {
      this.form = this._FB.group({
        name: [''],
        email: [''],
        password: [''],
        dob: [''],
        address:[''],
        lat:[''],
        lng:['']
     });
  }

  ionViewDidLoad() {

  }

 
  register(val){
    this.authServ.createAccount(val).then((res) => {
      this.navCtrl.setRoot(LoginPage);
    }, (err) => {
      console.log("Error")
    })
  }

}

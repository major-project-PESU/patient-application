import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { ScheduleProvider } from '../../providers/schedule/schedule'
import { Schedule_2AddPage } from '../schedule-2-add/schedule-2-add';


@IonicPage()
@Component({
  selector: 'page-schedule-add',
  templateUrl: 'schedule-add.html',
})
export class ScheduleAddPage {
  public form 	: FormGroup;
  public today:any;
  public date:any;
  public maxDate:any;

  days: any = [];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private _FB          : FormBuilder,
    public scheduleServ: ScheduleProvider
    ) {
      this.date = new Date();
      this.today = this.date.toISOString();
      this.form = this._FB.group({
        medicines     : this._FB.array([
           this.initMedicinesField()
        ])
     });
  }

  setTimeFields(controls){
   //   console.log(instructions)
   let instructions = controls.instructions.value;
   let times = instructions.split("");

   if(parseInt(times[0]) > 0){
      controls.morningValid.value = true;
   }else{
      controls.morningValid.value = false;

   }

   if(parseInt(times[1]) > 0){
      controls.afternoonValid.value = true;
   }else{
      controls.afternoonValid.value = false;

   }

   if(parseInt(times[2]) > 0){
      controls.nightValid.value = true;
   }else{
      controls.nightValid.value = false;

   }
   // console.log(controls.morningValid)
   // if()
  }



  initMedicinesField() : FormGroup
{
   return this._FB.group({
      name : ['', Validators.required],
      morningTime : [''],
      afternoonTime: [''],
      nightTime:[''],
      instructions : ['',Validators.compose([Validators.required, Validators.pattern("[0-9]|[0-9]|[0-9]")])],
      endDate : ['',Validators.required],
      morningValid:[false],
      afternoonValid:[false],
      nightValid:[false]
   });
}


addNewInputField() : void
{
   const control = <FormArray>this.form.controls.medicines;
   control.push(this.initMedicinesField());
}

removeInputField(i : number) : void
{
   const control = <FormArray>this.form.controls.medicines;
   control.removeAt(i);
}

manage(val : any) : void
{
   
   this.navCtrl.push(Schedule_2AddPage,{data:val})
   // this.scheduleServ.addMedicine(val).then((res) => {
   //    this.navCtrl.pop();
   // }, (err) => {
   //    console.log(err)
   // })
}

   

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScheduleAddPage');
  }

}

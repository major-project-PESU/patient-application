import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LandingPage } from '../landing/landing';
import { RegisterPage } from '../register/register';
import { AuthProvider } from '../../providers/auth/auth';
// import { EmailValidator } from '@angular/forms';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email:String;
  password:String;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public authServ : AuthProvider
    ) {
  }

  ionViewDidLoad() {
    this.authServ.checkAuthentication().then(res => {
      this.navCtrl.setRoot(LandingPage);
    }, err => {
      console.log(err)
    })
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    let details = {
      email:this.email,
      password:this.password
    }
    this.authServ.login(details).then((res) => {
      this.navCtrl.setRoot(LandingPage);
    }, (err) => {
      console.log(err)
    })
  }

  goToRegisterPage(){
    this.navCtrl.push(RegisterPage);
  }
}

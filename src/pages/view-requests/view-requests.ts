import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BiddingProvider } from '../../providers/bidding/bidding';
import { ViewBidsPage } from '../view-bids/view-bids';
import { ViewProformaCheckoutPage } from '../view-proforma-checkout/view-proforma-checkout';
/**
 * Generated class for the ViewRequestsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-requests',
  templateUrl: 'view-requests.html',
})
export class ViewRequestsPage {

  
  public requests:any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public biddingServ:BiddingProvider) {
  }

  goToViewBidsPage(request){
      this.navCtrl.push(ViewBidsPage,{request:request});
  }

  ionViewDidEnter(){
    this.biddingServ.patientGetPresentBids().then((res) => {
      this.requests = res;
      console.log(res);
    }, (err) => {
      console.log(err);
    })
  }

}

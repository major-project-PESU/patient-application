import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ImageChooseAddressPage } from '../image-choose-address/image-choose-address';

/**
 * Generated class for the ViewPrescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-prescription',
  templateUrl: 'view-prescription.html',
})
export class ViewPrescriptionPage {


  public image:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.image = this.navParams.get('image');
    console.log(this.image)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPrescriptionPage');
  }

  goToChooseAddressPage(){
    this.navCtrl.push(ImageChooseAddressPage, {image:this.image})
  }
}

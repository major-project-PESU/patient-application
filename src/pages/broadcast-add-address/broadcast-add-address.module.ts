import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BroadcastAddAddressPage } from './broadcast-add-address';

@NgModule({
  declarations: [
    BroadcastAddAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(BroadcastAddAddressPage),
  ],
})
export class BroadcastAddAddressPageModule {}

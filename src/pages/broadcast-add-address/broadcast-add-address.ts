import {
  Component,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import {
  Geolocation
} from '@ionic-native/geolocation';
// import {GoogleMapsEvent} from '@ionic-native/goog'
import {
  NativeGeocoder,
} from '@ionic-native/native-geocoder';

import {
  AuthProvider
} from '../../providers/auth/auth';
import {
  PatientProvider
} from '../../providers/patient/patient';
/**
 * Generated class for the BroadcastAddAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-broadcast-add-address',
  templateUrl: 'broadcast-add-address.html',
})
export class BroadcastAddAddressPage {

  public medicines: any = [];
  @ViewChild('map') mapElement: ElementRef;

  map: any;
  public lat: any;
  public lng: any;
  public address: any;
  public city: any;
  public locality: any;
  public postalCode: any;
  public marker = null;
  public data: any;
  public addressName: any;
  public valid = false;
  public geocoder = new google.maps.Geocoder();
  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public authServ: AuthProvider, public geolocation: Geolocation,
    private _GEOCODE: NativeGeocoder, public patientServ: PatientProvider,
    public alertCtrl:AlertController) {
    this.medicines = this.navParams.get('medicines');
  }

  isValid(){
    this.valid = false;
    console.log("Validating")
    if(this.address && this.addressName && this.city && this.locality && this.postalCode){
      this.valid = true;
    }
  }

  confirmAlert(){
    let alert = this.alertCtrl.create({
      title:"Confirm Address",
      message:"The address to be added is : "+this.address,
      buttons : [
        {
          text: "No",
          role:'cancel',

        },
        {
          text: "Yes",
          handler: () => {
            this.addAddress();
          }
        }
      ]
    });
    alert.present();
  }

  setLocation() {
    let options = {
      timeout: 30000,
      enableHighAccuracy: true
    }
    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.lat = (resp.coords.latitude);
      this.lng = (resp.coords.longitude);
      var self = this;
      let latLng = new google.maps.LatLng(this.lat, this.lng);
      if (this.marker) {
        this.marker.setPosition(latLng);
      } else {
        this.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng,
          draggable: true
        });
        google.maps.event.addListener(this.marker, 'dragend', () => {
          this.lat = this.marker.getPosition().lat();
          this.lng = this.marker.getPosition().lng();
          // console.log(this.lat,this.lng);
          this.geocoder.geocode({
            "location": new google.maps.LatLng(this.lat, this.lng)
          }, function (res, status) {
            if (status.toString() != "OK") {
              self.address = "Nada"
            }
            self.address = (res[0].formatted_address);
          })
          this._GEOCODE.reverseGeocode(this.lat, this.lng).then((res) => {
            this.city = res[0].locality;
            this.locality = res[0].subLocality;
            this.postalCode = res[0].postalCode
          }, (err) => {
            console.log(err)
          })
        });
      }
      this.map.setCenter(latLng);
      this.geocoder.geocode({
        "location": latLng
      }, function (res, status) {
        if (status.toString() != "OK") {
          self.address = "Nada"
        }
        self.address = (res[0].formatted_address);
      })
      this._GEOCODE.reverseGeocode(this.lat, this.lng).then((res) => {
        this.city = res[0].locality;
        this.locality = res[0].subLocality;
        this.postalCode = res[0].postalCode
      }, (err) => {
        console.log(err)
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  loadMap() {

    let latLng = new google.maps.LatLng(-34.9290, 138.6010);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.setLocation()
    console.log(this.address);

  }

  addAddress() {
    let json = {
      name: this.addressName,
      fullAddress: this.address,
      city: this.city,
      locality: this.locality,
      postalCode: this.postalCode,
      lat: this.lat,
      lng: this.lng
    }
    this.patientServ.addAddress(json).then((res) => {
      this.navCtrl.pop();
    }, (err) => {
      console.log(err)
    })
  }
  ionViewDidLoad() {
    this.loadMap();
    console.log('ionViewDidLoad BroadcastAddAddressPage');
  }

}

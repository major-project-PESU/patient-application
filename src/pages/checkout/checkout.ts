import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { BroadcastChooseAddressPage } from '../broadcast-choose-address/broadcast-choose-address';

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  constructor(public navCtrl: NavController, public navParams: NavParams, public cartServ:CartProvider) {
  }

  goToChooseAddressPage(){
    this.navCtrl.push(BroadcastChooseAddressPage)
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }

  
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { SchedulePage } from '../schedule/schedule';
import { OrdersPage } from '../orders/orders';
import { HomePage } from '../home/home';
import { ActionSheetController , ModalController} from 'ionic-angular';
import { Camera } from '@ionic-native/camera';


@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  homePage:any;
  profilePage:any;
  schedulePage:any;
  ordersPage:any
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public modalCtrl:ModalController
    ) {
    this.homePage = HomePage;
    this.profilePage = ProfilePage;
    this.schedulePage = SchedulePage;
    this.ordersPage = OrdersPage;
  }

  newOrderOptions(){
    const actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Upload From Gallery',
          handler: () => {
            console.log(this.camera.PictureSourceType.CAMERA);

            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },{
          text: 'Upload from Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  } 

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
  
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      let modal = this.modalCtrl.create('UploadModalPage', { data: imagePath });
      modal.present();
      modal.onDidDismiss(data => {
        console.log("GONE")
      });
    }, (err) => {
      console.log('Error: ', err);
    });
  }
}


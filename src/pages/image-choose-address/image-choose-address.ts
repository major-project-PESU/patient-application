import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PatientProvider } from '../../providers/patient/patient';
import { BroadcastAddAddressPage } from '../broadcast-add-address/broadcast-add-address';
import { RequestProvider } from '../../providers/request/request';
import { LandingPage } from '../landing/landing';

/**
 * Generated class for the ImageChooseAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-image-choose-address',
  templateUrl: 'image-choose-address.html',
})
export class ImageChooseAddressPage {

  public addresses = [];
  public address:any;
  public image : any;
  public urgent: any;
  public by:any;
  public valid = false;
  constructor(public navCtrl: NavController, public alertCtrl:AlertController, public navParams: NavParams, public patientServ:PatientProvider, public requestServ:RequestProvider) {
    this.image = this.navParams.get('image')
  }

  confirmAlert(){
    let alert = this.alertCtrl.create({
      title:"Confirm Order",
      message:"Do you wish to confirm the order details and place an order? Note : This can't be undone",
      buttons : [
        {
          text: "No",
          role:'cancel',

        },
        {
          text: "Yes",
          handler: () => {
            this.broadcast();
          }
        }
      ]
    });
    alert.present();
  }

  goToAddAddressPage(){
    this.navCtrl.push(BroadcastAddAddressPage);
  }

  getAddress(){
    this.patientServ.getAddress().then((res) => {
      this.addresses = []
      for(var i in res){
        this.addresses.push(res[i])
      }
      console.log(this.addresses)
    }, (err) => {
      console.log(err)
    })
  }

  

  broadcast(){
    let json = {
      prescriptionID:this.image['_id'],
      url:this.image['url'],
      name:this.image['name'],
      patientName:this.image['patientName'],
      address:this.address,
      urgent:this.urgent,
      by:this.by
    }
    console.log(json)
    this.requestServ.checkoutByImage(json).then((res) => {
      this.navCtrl.setRoot(LandingPage);
    }, (err) => {
      console.log(err)
    })
  }

    
  isValid(){
    // console.log("VALIDATING");
    this.valid = false;
    if(!this.urgent){
      if(this.address){
        this.valid = true;
      }
    }else{
      if(this.address && this.by){
        this.valid = true
      }
    }
  }
  
  ionViewDidEnter(){
    this.getAddress();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ImageChooseAddressPage');
  }

}

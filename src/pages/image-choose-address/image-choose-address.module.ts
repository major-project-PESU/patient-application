import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImageChooseAddressPage } from './image-choose-address';

@NgModule({
  declarations: [
    ImageChooseAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(ImageChooseAddressPage),
  ],
})
export class ImageChooseAddressPageModule {}

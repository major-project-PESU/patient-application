import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { OrderProvider } from '../../providers/order/order';
import { ViewProformaCheckoutPage } from '../view-proforma-checkout/view-proforma-checkout';
import { TrackPage } from '../track/track';

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

  public filterPrescriptionBy = 1;
  public allOrders:any;
  public orders:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public orderServ:OrderProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersPage');
  }

  ionViewDidEnter(){
    this.getOrders();

  }

  getOrders(){
     this.orderServ.patientGetAllOrders().then((res) => {
      this.allOrders = res;
      // this.orders = this.allOrders;
      this.filter();
      console.log(res)
    }, err => {
      console.log(err)
    })
  }

  goToViewProformaInvoice(order){
    this.navCtrl.push(ViewProformaCheckoutPage,{bid:order});
  }

  goToTrackPage(order){
    this.navCtrl.push(TrackPage,{order:order});
  }
  goToCart(){
    this.navCtrl.push(CartPage);
  }

  filter(){
    function typeCompare(type){
      return function(element){
        return element.status === parseInt(type);
      }
    }
    this.orders = this.allOrders.filter(typeCompare(this.filterPrescriptionBy))

  }

}
